# Changelog

Toutes modifications et changements dans ce projet sera documenté ici !

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Version 0.1.0

### Classes

- Implémentation des méthodes de la classe Board
- Implémentation de la méthode init pour initaliser le tableau
- Implémentation de la méthode clear pour netoyer le tableau
- Implémentation de la mérhode load pour charger les status des intsection s'il y a une pierre ou pas
- Implémentation de la méthode set color pour définir une couleur de la pierre à une intersection
- Implémentation de la méthode color_at pour afficher la couleur actuelle dans une intersection
- Début de l'implémentation de la méthode stone_group_at

### Test
- Mises en place des tests unitaire automatisé avec les pipelines
- Mises en place des test des méthode dans le ficheir test/test_board.py
