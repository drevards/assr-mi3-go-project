'''The board module.

This module is in charge of the very basic handling of the go board.
'''
from collections import namedtuple
from enum import Enum


class Color(Enum):
    '''The color of a go board intersection.'''
    EMPTY = 0
    BLACK = 1
    WHITE = 2

def color_fron_char(c : str) -> Color:
 if c == "-":
     return Color.EMPTY
 elif c == "b":
     return Color.BLACK
 elif c == "w":
     return Color.WHITE
 else:
     raise ValueError("Unknown color")



def color_fron_char(c : str) -> Color: # Cette fonction est utilisé pour retourner en fonction du caractère trouvé, la couleur de l'intersection.

 if c == "-":
     return Color.EMPTY
 elif c == "b":
     return Color.BLACK
 elif c == "w":
     return Color.WHITE
 else:
     raise ValueError("Unknown color")

Intersection = namedtuple('Intersection', ['x', 'y'])


class Board:
    '''The main class to manage a go board.'''
    def __init__(self, size: int) -> None:
        '''Initialize an empty board of size*size intersections.'''

        self.size = size # initialisation de la taille
        self.board = [[Color.EMPTY for x in range(self.size)] for y in range(self.size)] #le plateau est représenté par une matrice de size * size, le tableau est initialisé vide avec aucune couleur entre les intersection Class.EMPTY<
        print('tab board : ',self.board)

    def clear(self) -> None:
        '''Clear all intersections such that they all become empty.'''
        for x in range(self.size):# on parcour la ligne
            for y in range(self.size):# on parcour la colone
                self.set_color(Color.EMPTY, x ,y)# chaque caractére de position (x,y) est changer pour que la couleur soit empty

    def load(self, board_state: str) -> None:
        '''Sets all intersection colors from board_state.

        board_state is a string that contains size*size characters (excluding spacing characters).
                    each character represents the color of an intersection ('-', 'b' or 'w').
        '''

        '''BLACK = b
        WHITE = w
        EMPTY = -'''


        lines = board_state.split("\n") # Va spliter et mettre en place le statut du tableau
        k = 0
        print("size", self.size, "lines", len(lines)) # Test affichage du nombre de ligne et de la taille
        tab_char = [] # Tableau avec le statut des couleurs des intersection en caractère. Test
        tab_state = [] # Tableau avec le statut des couleurs des intersection. Test
        if self.size != len(lines):
            raise ValueError("size must be same with the number of chraracer in board_state") # émet une erreur sir la taille du tableau n'est pas la même que la taille dans states
        for line in lines: #Parcours les lignes du tableau
            clean_line = line.strip() # Supprime les espaces dans lignes

            for caracter in clean_line: #Parcours des colonnes tableau

                y = k//self.size # Y représente les lignes
                x = k%self.size # X représente les colones
                add_char = self.set_color(color_fron_char(caracter), x, y) # Affichage du caractère correspondant à la couleur. Test
                print("add char :", add_char)

                tab_char.append(add_char) # Ajout des statut de couleurs (caractères) Test
                tab_state.append(color_fron_char(caracter)) # Ajout des statut de couleurs (Classe couleurs) Test
                self.board[x][y] = color_fron_char(caracter) # Ajout des statut dans le tableau principal en fonction de leur position (x,y)
                k += 1
                print('caractères : ',caracter)



                '''if  tab_board_state[x][y] == "b":
                    self.set_color(Color.BLACK, x, y) # attribut a la case la couleur correspondant

                if  tab_board_state[x][y] == "w":
                    self.set_color(Color.WHITE, x, y)

                if  tab_board_state[x][y ]== "-":
                    self.set_color(Color.EMPTY, x, y)'''


        print('tab caractères : ',tab_char)# Afficher le tableau test des statut (caractère)
        print('tab couleur : ',tab_state)# Afficher le tableau test des statut (classe couleurs)
        print('tab board : ',self.board)# Afficher le tableau principal des statuts



    def set_color(self, color: Color, x: int, y: int) -> None:
        #Set the color of the (x,y) intersection of the board.

        # on retourne le caractére en focntionne de la couleur qui est retrouvé dans le tableau board
        if color == Color.BLACK:
            self.board[x][y] = Color.BLACK # ajout a l'intersection (x,y) de la couleur black
        if color == Color.WHITE:
            self.board[x][y] = Color.WHITE # ajout a l'intersection (x,y) de la couleur white
        if color == Color.EMPTY:
            self.board[x][y] = Color.EMPTY # ajout a l'intersection (x,y) de la couleur black


    def color_at(self, x: int, y: int) -> Color:
        '''Get the color of the (x,y) intersection of the board.'''
        return self.board[x][y] # Color at retournera la couleur acuelle situé à l'endroi suivant

    def stone_group_at(self, x: int, y: int) -> [Intersection]:
        '''Get the maximum-sized stone group that starts from the (x,y) intersection of the board.'''
        for x in range(self.size):
            for y in range(self.size):
                if self.color_at(x,y) == self.color_at(x+1,y) or self.color_at(x,y) == self.color_at(x,y+1):
                    print("couleur voisine a :" , x, y)
