'''The board module.

This module is in charge of the very basic handling of the go board.
'''
from collections import namedtuple
from enum import Enum


class Color(Enum):
    '''The color of a go board intersection.'''
    EMPTY = 0
    BLACK = 1
    WHITE = 2


Intersection = namedtuple('Intersection', ['x', 'y'])


class Board:
    '''The main class to manage a go board.'''
    def __init__(self, size: int) -> None:
        '''Initialize an empty board of size*size intersections.'''

        raise NotImplementedError

    def clear(self) -> None:
        '''Clear all intersections such that they all become empty.'''
        raise NotImplementedError

    def load(self, board_state: str) -> None:
        '''Sets all intersection colors from board_state.

        board_state is a string that contains size*size characters (excluding spacing characters).
                    each character represents the color of an intersection ('-', 'b' or 'w').
        '''
        raise NotImplementedError

    def set_color(self, color: Color, x: int, y: int) -> None:
        '''Set the color of the (x,y) intersection of the board.'''
        raise NotImplementedError

    def color_at(self, x: int, y: int) -> Color:
        '''Get the color of the (x,y) intersection of the board.'''
        raise NotImplementedError

    def stone_group_at(self, x: int, y: int) -> [Intersection]:
        '''Get the maximum-sized stone group that starts from the (x,y) intersection of the board.'''
        raise NotImplementedError
