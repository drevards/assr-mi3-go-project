from gomi3.board import *


def test_load(): # test methode load de board.py
    b = Board(3)
    # Inistialistation du tableu
    state = (
    '''---
    -w-
    b--''')
    test_b = state.split("\n") # Test de la focntion split
    print(test_b)
    print(len(test_b))
    b.load(state)


    for y in range(3): # Boucle de test pour vérifier si la couleur attendu à une intersection correspond à la couleur acuelle.
        for x in range(3):
            print(x, y)
            if ((x,y) == (0,2)):
                print("Color in 0,2",b.color_at(x,y))
                assert b.color_at(x,y) == Color.BLACK, "(x=0,y=2) should be black" # Vérifie si la couleur du tableau principal renvoie bien la bonne couleur.
            elif ((x,y) == (1,1)):
                print("Color in 1,1",b.color_at(x,y))
                assert b.color_at(x,y) == Color.WHITE, "(x=1,y=1) should be white"
            else:
                assert b.color_at(x,y) == Color.EMPTY


def test_base():# test de set_color de board.py

    b = Board(3)
    b.set_color(Color.BLACK, 0,0)

    for y in range(3):
        for x in range(3):
            if ((x,y)==(0,0)):
                assert b.color_at(x,y) == Color.BLACK
            else:
                assert b.color_at(x,y) == Color.EMPTY


def test_clear():# test de clear de board.py
    b = Board(3)
    b.set_color(Color.BLACK, 0,0)#ajout de couleurs dans différente case de noter tableau
    b.set_color(Color.WHITE, 1,1)
    b.set_color(Color.BLACK, 1,2)


    b.clear()# utilisation de la methode clear pour vider notre tableau


    for y in range(3):
        for x in range(3):
            if ((x,y)==(0,0)):
                assert b.color_at(x,y) == Color.EMPTY
            if ((x,y)==(1,1)):
                assert b.color_at(x,y) == Color.EMPTY
            if ((x,y)==(1,2)):
                assert b.color_at(x,y) == Color.EMPTY
