assr-mi3-go-project - Projet du jeu de go en Python
===================

Il a était mis en place un script GitLab CI permettant d'automatisé l'analyse statique du projet et la mise en place des tests ainsi que la génération de rapports d'analyse et la couvertur de nos tests.

Le script Gitlab échou suite a une erreur d'implémentation dans le script python test_board.py

Le coverage était a 55%.

chaque methodes du fichier board.py est ajouté/modifié depuis une branche au nom de cette méthode. de même pour les fonction de test du fichier test_board.py. 

Des implementaions de code au niveau du fichier board.py ont était faite pour les méthodes __init__, load, color_at, set_color.

----------------------------------------------------------------------------------------------

Dans la branche load on implémente la méthode load du fichier board.py ainsi que la focntion test_load du fichier test_board.py

La méthode load permet d'ajouter au tableau self.board toutes les couleurs d'intersection de board_state, qui est une chaine de caractére qui contiens des caractére de size*size chaque carctéres représente la couleur de l'intersection('-','b' ou 'w').
Il y a trois caractères qui défini l'état des intersections:
    -   Le caractère "-" signifie que l'intersection est vide et revoie l'état Color.EMPTY.
    -   Le caractère "b" signifie la présence de la pierre noire sur l'intersection, il revoie alors l'état Color.BLACK
    -   La caractère "w" signifie la présence de la pierre blanche sur l'intersection, il revoie alors l'état Color.WHITE
En fonction des caractère touvée dans le tableau intégré board_sate, la métohde va générer un tableau avec le statut des couleurs provenant depuis la classe Color, et un autre tableau de statut des couleurs en prenant les caractères "-,b,w". puis il paroureras les colones des tableau en ajoutant les statut des insesection en fonction de la dimension du tableau. pui les ajouteras au tableau self.board une fois que les étas des intersections sont chargée.

----------------------------------------------------------------------------------------------

Dans la branche color_at, on implemente la methode color_at. la methode color_at est utilisé et tester pour chacune des fonction test de methode.

La méthode color_at, elle permet de récupérer les couleurs a l'intersection (x,y) du tableau.

----------------------------------------------------------------------------------------------

Dans la branche set_color, on implemente la méthode set_color du fichier board.py, on a églement créer un fonction dans le fichier test_board.py qui permet de tester cette méthode, le nom de la fonction est test_base.

La méthode set_color, elle permet d'ajouter les couleurs aux intersections (x,y) du plateau.

----------------------------------------------------------------------------------------------

Dans la branche clear, on implemente la méthode clear du fichier board.py, on a églement créer une fonction dans le fichier test_board.py qui permet de tester cette méthode, le nom de la fonction est test_clear.

La méthode clear permet de réinitialiser le tableau des intersections, en mettant qu'il n'y as pas de pierre dans tout les intersections (Color.EMPTY).

----------------------------------------------------------------------------------------------

On a merge depuis la branche main toutes les branches qui ont pu être créés au cours de ce projet.

Ansi depuis la branche main, il est possible de retrouver l'intégralité de notre programme.

Pour tester les methodes qui sont implementé il suffit de lancer le fichier .gitlab-ci.yml.

Le coverage est de 88 % avec tout les test de code sont correcte.
